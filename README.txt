
                          Create and Link Plugin


                       Matt Doar, mdoar@pobox.com
                          Consulting Toolsmiths


Description
-----------

This plugin lets you add a new operation to JIRA to create a new issue and
also create a link back to the current issue. It's like creating a
subtask, but using links instead.

The new issue can have the priority and other fields set to defaults, and
various fields (including custom fields) can be inherited from the
current issue.

Usage
-----

   1. Check the version to use and download the plugin JAR file from
      the release location. Install it into the version 2 plugins
      directory location in {jira.home}/plugins/installed-plugins

   2. Also download the file createandlink.jsp and install it in the directory
      atlassian-jira/secure/custom/com/consultingtoolsmiths/jira/plugins/createandlink
      Create this directory if it doesn't already exist.

   3. Restart JIRA

   4. Make sure that issue linking is enabled.

   5. Go to Admin, Plugins, Create and Link and define a new Operation.
      An example operation would be "Create and Link a Bug", "inherit",
      "inherit" and some link type such as "Related"". 

      If you select "Choose when used", then you will be prompted for
      the project, issue type or link type before being presented with
      the issue fields.

      The arguments field here is where you define what is copied from
      the original issue and what is preset.
   
   6. Now when you view an issue, you should see one a operation under
      More Actions named "Create and Link"
a
   7. When you click on the link, you are taken to a screen where you
      can choose from the operations defined in (5) and enter the
      project, issue type and link type as necessary.

      If you want to prepopulate these fields, then lines in the Admin
      arguments field such as "summary=inherit" or "summary=A New Bug"
      will do that.


Testing
-------

   1. Check that the expected link appears in the More Actions menu for an issue.
   2. There may be an existing issue already filed at
      https://studio.plugins.atlassian.com/browse/JCLP

   3. If not, please create an issue with the JIRA and plugin versions and what
      steps you performed, what happened and what you expected to happen.
      

Troubleshooting
---------------

1. If you get an error when you TODO, you may not have the
createandlink.jsp file installed, or it may be in the wrong
location. It should also be the correct version for the plugin.

2. Adding the following lines to
atlassian-jira/WEB-INF/classes/log4j.properties and restart JIRA. This
will make the plugin run a little slower.

log4j.category.com.consultingtoolsmiths.jira.plugins.createandlink.Operation = DEBUG, console, filelog
log4j.additivity.com.consultingtoolsmiths.jira.plugins.createandlink.Operation = false
log4j.category.com.consultingtoolsmiths.jira.plugins.createandlink.rest.IssueTypesService = DEBUG, console, filelog
log4j.additivity.com.consultingtoolsmiths.jira.plugins.createandlink.rest.IssueTypesService = false
log4j.category.com.consultingtoolsmiths.jira.plugins.persist.Persister = DEBUG, console, filelog
log4j.additivity.com.consultingtoolsmiths.jira.plugins.persist.Persister = false
log4j.category.com.atlassian.jira.web.action.custom.CreateAndLink = DEBUG, console, filelog
log4j.additivity.com.atlassian.jira.web.action.custom.CreateAndLink = false
log4j.category.com.consultingtoolsmiths.jira.plugins.createandlink.CreateAndLinkAdminAction = DEBUG, console, filelog
log4j.additivity.com.consultingtoolsmiths.jira.plugins.createandlink.CreateAndLinkAdminAction = false
log4j.category.com.atlassian.jira.web.action.custom.CreateAndLink = DEBUG, console, filelog
log4j.additivity.com.atlassian.jira.web.action.custom.CreateAndLink = false
