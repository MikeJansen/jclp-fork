package com.consultingtoolsmiths.jira.plugins.persist;

import java.util.Map.Entry;
import org.w3c.dom.*;

/**
 * Classes that can be stored should implement this interface.
 */
public interface Persistable {

    /**
     * Create an element with a suitable name for the implementing class.
     * Add attributes for key, value pairs and other children as
     * desired to the element. The unpacking from XML is done by the
     * constructor that takes a Node argument.
     */
    public Element getXML(Document doc);
    
    /**
     * Define a constructor to create a new Object from an XML Node.
     * Make sure that this sets whatever is returned by getKey().
     * The schema of the XML is defined by getXML(). If a value is no
     * longer valid, throw an exception in the constructor
     */
    //public MyPersistable(Node node);
    
    /**
     * Return a key suitable for a Map. Call only after the key has been set.
     */
    public Object getKey();
    
}