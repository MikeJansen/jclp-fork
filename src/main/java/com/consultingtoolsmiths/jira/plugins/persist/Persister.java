package com.consultingtoolsmiths.jira.plugins.persist;

import com.opensymphony.module.propertyset.PropertyException;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.module.propertyset.PropertySetManager;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.*;
import java.util.*;
import java.util.Map.Entry;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.*;

/**
 * A class for storing information to the database. Useful for plugin
 * configuration data. The data is converted to XML, then to a String
 * representation and stored as text. The String representation is
 * also useful for saving as a configuration file outside JIRA.
 */
public class Persister {

    public static Log log = LogFactory.getLog(Persister.class);

    private static PropertySet ofbizPs = null;

    /**
     * The entityId value in propertyentry
     */
    private int entityId;

    /**
     * The entityName value in propertyentry and also used in the XML
     */
    private String entityName;
    
    /**
     * The version identifier from the class using this persister.
     */
    private String version;

    private Persister() {}

    public Persister(int entityId, String entityName, String version) {
        this.entityId = entityId;
        this.entityName = entityName;
        this.version = version;
        getPS();
    }

    /**
     * Use the propertyentry and propertytext tables to store the
     * configuration information. This is the same place that
     * PortletConfiguration is saved. 
     */
    private PropertySet getPS() {
        if (ofbizPs == null) {
            HashMap ofbizArgs = new HashMap();
            ofbizArgs.put("delegator.name", "default");
            ofbizArgs.put("entityName", entityName);
            ofbizArgs.put("entityId", new Long(entityId));
            // See propertyset.xml for choices of types here
            ofbizPs = PropertySetManager.getInstance("ofbiz", ofbizArgs);
        }
        return ofbizPs;
    }

    /**
     * Persist a Map to the database using the given key
     * @return false on failure 
     */
    public boolean saveMap(String key, Map map) {
        Document doc = mapToDoc(map);
        if (doc == null) {
            return false;
        }
        String docStr = docToString(doc);
        if (docStr == null) {
            return false;
        }
        log.debug("Saving map as:\n" + docStr);

        // Note: setXML generated unimplemented exception and setString has
        // a max length of 255 characters. Remember if you change the
        // type that is saved to delete the entry in propertyentry or
        // getX won't work.
        getPS().setText(key, docStr);
        return true;
    }
    
    /**
     * Load a Map from the database. Return null for no data.
     *
     * @param key The key value in the property key field in the
     * property entry table
     * @params persistable The class of the objects that will be
     * loaded into the Map. This class must implement Persistable.
     * @return A Map of string keys in the database to objects that
     * implement Persistable
     */
    public Map<String, Persistable> loadMap(String key, Class persistable) {
        try {
            String s = getPS().getText(key);
            if (s != null) {
                log.debug("Found saved data for the key: " + key + "\n" + s);
                Map<String, Persistable> m = stringToMap(s, persistable);
                return m;
            } else {
                log.debug("No saved data for the key: " + key);
            }
        } catch (Exception pe) {
            log.debug("No data stored in the database yet for key " + key);
            // pe.printStackTrace();
        }        
        return null;
    }
    
    /**
     * Return an XML Document representation of the given Map of
     * objects that have a getXML() method.
     */
    protected Document mapToDoc(Map map) {
        try {
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc  = db.newDocument();
            Element root = doc.createElement(entityName);
            root.setAttribute("version", version);
            doc.appendChild(root);
            Iterator it = map.entrySet().iterator();
            while(it.hasNext()) {
                Entry entry = (Entry) it.next();
                Element e = ((Persistable)entry.getValue()).getXML(doc);
                root.appendChild(e);
            }        
            return doc;
        } catch (ParserConfigurationException pce) {
            log.error("Problem creating XML Document:");
            pce.printStackTrace();
            return null;
        } 
    }
    
    /**
     * Transform the Document into a String
     */
    protected String docToString(Document doc) {
        try {
            DOMSource domSource = new DOMSource(doc);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            //transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.ENCODING,"ISO-8859-1");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            java.io.StringWriter sw = new java.io.StringWriter();
            StreamResult sr = new StreamResult(sw);
            transformer.transform(domSource, sr);
            String xml = sw.toString();
            return xml;
        } catch (Exception ex) {
            log.error("Unable to create String from XML Document");
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Return a Map populated from the given String that is the
     * representation of an XML document.
     *
     * @param key the name of the child elements to be added to the Map
     */
    protected Map<String, Persistable> stringToMap(String s, Class persistable) {
        Map map = new HashMap(); // TODO or TreeMap?
        try {
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputStream is = new ByteArrayInputStream(s.getBytes("UTF-8"));
            
            Document doc = db.parse(is);
            String dataVersion = doc.getDocumentElement().getAttributes().getNamedItem("version").getNodeValue();
            log.debug("Data version: " + dataVersion);
            // The child elements are always the same as the class name
            // The class name with periods may not be valid XML?
            String key = persistable.getName().replace('.', '_');
            NodeList ops = doc.getElementsByTagName(key);
            Class[] ctorArgs = new Class[2];
            ctorArgs[0] = String.class;
            ctorArgs[1] = Node.class;
            Constructor ctor = persistable.getConstructor(ctorArgs);
            for (int i=0; i<ops.getLength(); i++) {
                Node op = ops.item(i);
                Object[] args = new Object[2];
                args[0] = dataVersion;
                args[1] = op;
                try {
                    Persistable val = (Persistable) ctor.newInstance(args);
                    Object okey = val.getKey();
                    map.put(okey, val);
                } catch (Exception ex) {
                    log.warn("Operation " + i + " could not be created from the stored data:\n" + s);
                }
            }
            return map;
        } catch (Exception ex) {
            log.error("Unable to create Map from String of a XML Document");
            ex.printStackTrace();
            return map;
        }
    }

}