package com.consultingtoolsmiths.jira.plugins.createandlink.rest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.consultingtoolsmiths.jira.plugins.createandlink.CreateAndLinkAdminAction;
import com.consultingtoolsmiths.jira.plugins.createandlink.Operation;
import com.opensymphony.user.User;

@Path("operations")
public class OperationsService {

    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final IssueService issueService;
    private final PermissionManager permissionManager;
    private final ProjectManager projectManager;

    public OperationsService(JiraAuthenticationContext jiraAuthenticationContext, IssueService issueService,
            PermissionManager permissionManager, ProjectManager projectManager) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.issueService = issueService;
        this.permissionManager = permissionManager;
        this.projectManager = projectManager;
    }

    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public List<OperationWrapper> getOperations(@QueryParam("id") final String id) {
        List<OperationWrapper> wrappedOperations = new ArrayList<OperationWrapper>();
        Collection<Operation> operations = CreateAndLinkAdminAction.getAllOperationsSorted();
        User user = this.jiraAuthenticationContext.getUser();
        Issue issue = this.issueService.getIssue(user, Long.valueOf(id)).getIssue();
        /*
         * We know from the CreateAndLinkCondition that we have permission to
         * perform at least one of these operations. We just need to filter out
         * those we can not perform. FIXME - Perhaps it would be better to have
         * a service method that returned the filtered list rather than repeat
         * similar logic here and in the condition? -JMP
         */
        for (Operation operation : operations) {
            String tp = operation.getTargetProject();
            if (Operation.INHERIT.equalsIgnoreCase(tp)) {
                Project project = issue.getProjectObject();
                if (!this.permissionManager.hasPermission(Permissions.CREATE_ISSUE, project, user)) {
                    continue;
                }
            } else if (Operation.CHOOSE.equalsIgnoreCase(tp)) {
                if (this.permissionManager.getProjectObjects(Permissions.CREATE_ISSUE, user).isEmpty()) {
                    continue;
                }
            } else {
                Project project = this.projectManager.getProjectObj(Long.valueOf(tp));
                if (project == null || !this.permissionManager.hasPermission(Permissions.CREATE_ISSUE, project, user)) {
                    continue;
                }
            }
            wrappedOperations.add(new OperationWrapper(operation, issue, this.jiraAuthenticationContext));
        }
        return wrappedOperations;
    }
}
