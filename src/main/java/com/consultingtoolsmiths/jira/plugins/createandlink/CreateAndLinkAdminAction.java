package com.consultingtoolsmiths.jira.plugins.createandlink;

import com.consultingtoolsmiths.jira.plugins.persist.Persister;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import java.util.*;
import org.apache.log4j.Logger;
import webwork.action.ActionContext;

/**
 * A class to store system-wide information for a plugin.
 *
 * This information is also persisted to the database in the
 * PropertySet tables.
 */
public class CreateAndLinkAdminAction extends JiraWebActionSupport {

    public static Logger log = org.apache.log4j.Logger.getLogger(CreateAndLinkAdminAction.class);

    // This only has to change if the data you save changes, but not
    // necessarily with the plugin version
    private final IssueLinkTypeManager issueLinkTypeManager;

    public CreateAndLinkAdminAction(IssueLinkTypeManager issueLinkTypeManager) {
        this.issueLinkTypeManager = issueLinkTypeManager;
        // This value was selected by a process of pidooma. 
        // The next id is stored in OSPropertyEntry in SEQUENCE_VALUE_ITEM
        // TODO request a value properly?
        getPersister();
    }

    @RequiresXsrfCheck
    public String doDefault() {
        log.debug("Entering doDefault");
        return SUCCESS;
    }

    @RequiresXsrfCheck
    public String doAddOperation() {
        log.debug("Entering doAddOperation");
        currentOperation = null;
        return INPUT;
    }

    @RequiresXsrfCheck
    public String doEditOperation() {
        log.debug("Entering doEditOperation");
        final Map actionParams = ActionContext.getParameters();

        if (!actionParams.containsKey("opid")) {
            log.error("Expected a parameter: opid");
            return ERROR;
        }
        String[] values = (String[])actionParams.get("opid");
        String opid = values[0];
        if (opid == null) {
            addErrorMessage("No opid value specified");
            return ERROR;
        }

        log.debug("Editing operation id " + opid);
        Map ops = operations(true);
        if (!ops.containsKey(opid)) {
            String msg = "No operation with key " + opid + " exists";
            log.warn(msg);
            addErrorMessage(msg); // TODO this doesn't appear 
            return ERROR;
        }

        currentOperation = (Operation)ops.get(opid);
        return INPUT;
    }

    protected boolean validate(Map actionParams) {
        String[] expected = {"operationtext", "targetProject", "targetIssueType", "linkType", "arguments"};
        boolean result = true;
        for (int i=0; i < expected.length; i++) {
            String key = expected[i];
            if (!actionParams.containsKey(key)) {
                log.error("Expected a parameter: " + key);
                result = false;
            }
        }
        return result;
    }

    /**
     * Both AddOperation and EditOperation use the same createandlink_edit.vm
     * screen and submit their values to this method.
     */
    @RequiresXsrfCheck
    public String doModifyOperation() {
        log.debug("Entering doModifyOperation");

        final Map actionParams = ActionContext.getParameters();
        if (!validate(actionParams)) {
            return ERROR;
        }

        String[] values = (String[])actionParams.get("operationtext");
        String operationText = values[0];
        if (operationText == null || operationText.equals("")) {
            addErrorMessage("No operation text value specified");
            return ERROR;
        }

        values = (String[])actionParams.get("targetProject");
        String targetProject = values[0];
        if (targetProject == null) {
            addErrorMessage("No target project value specified");
            return ERROR;
        }

        values = (String[])actionParams.get("targetIssueType");
        String targetIssueType = values[0];
        if (targetIssueType == null) {
            addErrorMessage("No target issue type value specified");
            return ERROR;
        }

        values = (String[])actionParams.get("linkType");
        String value = values[0];
        if (value == null) {
            String msg = "No link type value specified";
            log.error(msg);
            addErrorMessage(msg);
            return ERROR;
        }
        String linkType = value;
        boolean isInward = false;
        if (value.equalsIgnoreCase("choose")) {
            // boolean isInward = false;  // irrelevant since it will be set later
        } else {
            String[] vals = value.split("_");
            if (vals.length != 2) {
                String msg = "Invalid link type value specified, expected id_direction format: " + value;
                log.error(msg);
                addErrorMessage(msg);
                return ERROR;
            }
            linkType = vals[0];
            isInward = vals[1].equalsIgnoreCase("inward");
        }
            
        Map<String, Operation> ops = operations(true);
        String opid;
        if (!actionParams.containsKey("opid")) {
            log.error("Expected a parameter opid");
            return ERROR;
        }
        values = (String[])actionParams.get("opid");
        opid = values[0];
        if (opid != null && !opid.equals("")) {
            log.debug("Updating an operation with id " + opid);
            if (!ops.containsKey(opid)) {
                // This can occur if a project, issue type or link type was
                // deleted and then the operation was removed because of that.
                log.warn("Could not find operation with id " + opid);
                return ERROR;
            }
            
            // Edit the existing Operation
            // TODO add more checking here
            Operation op = (Operation)ops.get(opid);
            op.setOperationText(operationText);
            op.setTargetProject(targetProject);
            op.setTargetIssueType(targetIssueType);
            op.setLinkType(linkType);
            op.setLinkDirection(isInward);
            op.setArguments(getArguments());

            // Persist the operations Map to the database
            if (!getPersister().saveMap(mapKey, ops)) {
                // TODO addErrorMessage(why)
                return ERROR;
            }
            return SUCCESS;
        }

        // Prevent duplicate operationText values to avoid confusion,
        // now that multiple operations don't appear in a single menu
        // section. 
        // TODO Even if they are created, the duplicates don't show up 
        // in the list of operations, probably due to using
        // getAllOperationsSorted
        boolean found = false;
        for (Operation op: ops.values()) {
            if (op.getOperationText().equalsIgnoreCase(operationText)) {
                found = true; // TODO could also record the value found
            }
        }
        if (found) {
            String msg = "An operation named " + operationText + " already exists, ignoring case";
            addErrorMessage(msg);
            return ERROR;
        }

        log.debug("Creating a new Operation");
        Operation operation = new Operation(targetProject, targetIssueType, linkType, isInward, operationText);

        // Has to be unique and valid in a URL
        Random generator = new Random();
        do {
            opid = Integer.toString(Math.abs(generator.nextInt()));
        } while (ops.containsKey(opid));
        operation.setId(opid);

        // Limit which projects etc can be used in operations
        // TODO not used yet
        Collection projects = new ArrayList();
        operation.setAllowedProjects(projects);
        Collection issuetypes = new ArrayList();
        operation.setAllowedIssueTypes(issuetypes);

        ops.put(opid, operation);

        String arguments = getArguments();
        if (arguments != null) {
            operation.setArguments(arguments);
        }

        // Persist the operations Map to the database
        if (!getPersister().saveMap(mapKey, ops)) {
            // TODO addErrorMessage(why)
            return ERROR;
        }

        return SUCCESS;
    }

    /**
     * Deleting an operation doesn't use an input screen
     */
    @RequiresXsrfCheck
    public String doDeleteOperation() {
        log.debug("Entering doDeleteOperation");

        final Map actionParams = ActionContext.getParameters();
        if (!actionParams.containsKey("opid")) {
            log.error("Expected a parameter: opid");
            return ERROR;
        }
        String[] values = (String[])actionParams.get("opid");
        String opid = values[0];
        if (opid == null) {
            addErrorMessage("No opid value was specified");
            return ERROR;
        }

        log.debug("Deleting operation id " + opid);
        Map<String, Operation> ops = operations(true);
        if (ops.remove(opid) == null) {
            log.warn("No operation with id " + opid + " was found for deletion");
        }

        // Persist the operations Map to the database
        if (!getPersister().saveMap(mapKey, ops)) {
            // TODO addErrorMessage(why)
            return ERROR;
        }

        return SUCCESS;
    }

    public Collection getProjects() {
        return ComponentManager.getInstance().getProjectManager().getProjects();
    }

    /**
     * Return all non sub-task issue types
     */
    public Collection getIssuetypes() {
        return ComponentManager.getInstance().getConstantsManager().getRegularIssueTypeObjects();
    }

    public Collection getLinktypes() {
        return issueLinkTypeManager.getIssueLinkTypes();
    }

    public boolean getIssueLinkingEnabled() {
        return getApplicationProperties().getOption(APKeys.JIRA_OPTION_ISSUELINKING);
    }
 
    private String arguments;

    public void setArguments(String arguments){
        this.arguments = arguments;
    }

    public String getArguments(){
        return arguments;
    }

    // Static methods and variables follow below here
    
    /**
     * A singleton only accessed via operations(boolean).
     * Maps the numeric operation id (String) to the Operation object
     */
    private static Map<String, Operation> operations = null;

    private static final String mapKey = "operations";

    /**
     * Accessor the singleton variable: operations
     *
     * @param isLoad if true, then reload from the database
     */
    private static Map<String, Operation> operations(boolean isLoad) {
        if (operations == null) {
            operations = new TreeMap<String, Operation>();            
        }
        if (isLoad) {
            getAllOperations();
        }

        // Check the current validity of each operation.
        List<String> tbd = new ArrayList<String>();
        for (Map.Entry<String, Operation> e : operations.entrySet()) {
            String key = e.getKey();
            Operation op = e.getValue();
            if (!op.isValid()) {
                tbd.add(key);
            }
        }
        if (!tbd.isEmpty()) {
            for (String key: tbd) {
                operations.remove(key);
            }
            if (!getPersister().saveMap(mapKey, operations)) {
                log.warn("Unable to save list of operations with invalid operations removed");
            }
        }

        return operations;
    }

    private static Persister persister = null;

    /**
     * 1.0 - original
     * 1.1 - JCLP-16 add a link direction
     */
    private final static String dataVersion = "1.1";
    
    /**
     * Accessor the singleton variable: persister
     */
    public static Persister getPersister() {
        if (persister == null) {
            persister = new Persister(15002, "createandlink", dataVersion);
        }
        return persister;
    }

    /**
     * @return A single Operation object, or null if there is none
     * with an id of opid
     */
    public static Operation getOneOperation(String opid) {
        return (Operation)(operations(false).get(opid));
    }

    /**
     * @return A Collection of the Operation objects
     */
    public static Collection<Operation> getAllOperations() {
        log.debug("Entering getAllOperations()");
        Map<String, Operation> ops = operations(false);
        if (ops.isEmpty()) {
            log.debug("Retrieving operations from database");
            // TODO add type here
            Map savedOperations = getPersister().loadMap(mapKey, Operation.class);
            if (savedOperations == null) {
                log.debug("No operations were found in the database");
            } else {
                log.debug("Found " + savedOperations.size() + " saved operations");
                // Add the retrieved operations to the local singleton Map
                ops.putAll(savedOperations);
            }
        } else {
            log.debug("Already got " + ops.size() + " operation(s)");
        }

        return ops.values();
    }

    public static Set<Operation> getAllOperationsSorted() {
        Collection<Operation> ops = getAllOperations();
        TreeSet<Operation> ts = new TreeSet<Operation>(new Operation.OperationComp());
        ts.addAll(ops);
        return ts;
    }

    // The current* methods are used to allow the same .vm to be used for 
    // adding and editing operations

    private Operation currentOperation;

    public String getCurrentId() {
        if (currentOperation != null) {
            return currentOperation.getId();
        }
        return "";
    }

    public String getCurrentOperationText() {
        if (currentOperation != null) {
            return currentOperation.getOperationText();
        }
        return "";
    }

    public String getCurrentTargetProject() {
        if (currentOperation != null) {
            return currentOperation.getTargetProject();
        }
        return "";
    }

    public String getCurrentTargetIssueType() {
        if (currentOperation != null) {
            return currentOperation.getTargetIssueType();
        }
        return "";
    }

    public String getCurrentLinkType() {
        if (currentOperation != null) {
            return currentOperation.getLinkType();
        }
        return "";
    }

    public String getCurrentLinkDirection() {
        if (currentOperation != null) {
            return currentOperation.getLinkDirection();
        }
        return "";
    }

    public String getCurrentArguments() {
        if (currentOperation != null) {
            return currentOperation.getArguments();
        }
        return "";
    }

}
