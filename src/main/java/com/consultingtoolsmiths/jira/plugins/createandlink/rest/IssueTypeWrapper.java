package com.consultingtoolsmiths.jira.plugins.createandlink.rest;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.atlassian.jira.issue.issuetype.IssueType;

@XmlRootElement(name = "issuetype")
public class IssueTypeWrapper {

	@XmlElement(name = "id")
	private String id;

	@XmlElement(name = "name")
	private String name;

	public IssueTypeWrapper() {}
	
	public IssueTypeWrapper(IssueType issueType) {
		this.id = issueType.getId();
		this.name = issueType.getName();
	}

	public String getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}
}
