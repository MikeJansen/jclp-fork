package com.consultingtoolsmiths.jira.plugins.createandlink.rest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;

import org.apache.log4j.Logger;

@Path("issuetypes")
public class IssueTypesService {

    public static Logger log = org.apache.log4j.Logger.getLogger(IssueTypesService.class);
    
    private IssueTypeSchemeManager issueTypeSchemeManager;
    private ProjectManager projectManager;
    
    public IssueTypesService(IssueTypeSchemeManager issueTypeSchemeManager,
                             ProjectManager projectManager) {
        this.issueTypeSchemeManager = issueTypeSchemeManager;
        this.projectManager = projectManager;
    }
    
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public List<IssueTypeWrapper> getIssueTypes(@QueryParam("pid") final String pid) {
        List<IssueTypeWrapper> wrappedIssueTypes = new ArrayList<IssueTypeWrapper>();
        Project project = this.projectManager.getProjectObj(Long.valueOf(pid));
        Collection<IssueType> issueTypes = this.issueTypeSchemeManager
            .getNonSubTaskIssueTypesForProject(project);
        for (IssueType issueType : issueTypes) {
            log.debug("Adding issue type " + issueType.getName() + " to values returned to client");
            wrappedIssueTypes.add(new IssueTypeWrapper(issueType));
        }
        return wrappedIssueTypes;
    }
}
