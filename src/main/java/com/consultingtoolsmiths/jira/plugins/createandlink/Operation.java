package com.consultingtoolsmiths.jira.plugins.createandlink;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.ManagerFactory;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.customfields.impl.CascadingSelectCFType;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.ofbiz.OfBizValueWrapper;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.web.util.OutlookDateManager;
import com.consultingtoolsmiths.jira.plugins.persist.Persistable;
import com.opensymphony.user.User;

import java.sql.Timestamp;
import java.util.*;
import java.util.Map.Entry;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.GenericValue;
import org.w3c.dom.*;

/**
 * A class representing a single issue operation.
 * If this class is changed, change OperationWrapper to match.
 */
public class Operation implements Persistable {

    public static Logger log = org.apache.log4j.Logger.getLogger(Operation.class);

    private final OutlookDateManager outlookDateManager;
    private final VersionManager versionManager;
    private final ProjectComponentManager projectComponentManager;

    /**
     * A unique identifier for each Operation object, valid over a
     * Tomcat session.
     */
    protected String id;

    /**
     * The preconfigured project id that the new issue will be created in.
     * Can be the same as the source issue's project ("inherit").
     */
    protected String targetProject;

    /**
     * The preconfigured issue type id that the new issue will be created as
     * Can be the same as the source issue's issue type ("inherit").
     */
    protected String targetIssueType;

    /**
     * The id of the link type
     */
    protected String linkType;

    /**
     * The direction of the link type
     * @since dataVersion 1.1
     */
    protected boolean linkInward;

    /**
     * The text that appears as part of the HTML link for the operation
     */
    protected String operationText;

    /**
     * The extra arguments that are passed when the issue operation is invoked
     */
    protected String arguments;

    /**
     * This operation will only be show for issues in projects in this list.
     * This list contains project ids.
     */
    protected Collection allowedProjects;

    /**
     * This operation will only be show for issues with issue types in this list
     * This list contains issue type ids.
     */
    protected Collection allowedIssueTypes;

    public static final String INHERIT = "inherit";
    private final String INHERIT_REPLACEMENT = INHERIT; // regex pattern
    private final String INHERIT_FROM_ISSUE = "Inherited from the parent issue";

    public static final String CHOOSE = "choose";
    private final String CHOOSE_WHEN_USED = "Chosen when used";

    /**
     * Default constructor, but note there is also another one.
     */
    public Operation(String targetProject, 
                     String targetIssueType, 
                     String linkType, 
                     boolean linkInward, 
                     String operationText) {
        // TODO check that the project and issue type and link type are still valid 
        this.targetProject = targetProject;
        this.targetIssueType = targetIssueType;
        this.linkType = linkType;
        this.linkInward = linkInward;
        this.operationText = operationText;
        // id and arguments are not set here
        issueLinkTypeManager = getIssueLinkTypeManager();

        this.outlookDateManager = ManagerFactory.getOutlookDateManager();
        this.versionManager = ComponentManager.getInstance().getVersionManager();
        this.projectComponentManager = ComponentManager.getInstance().getProjectComponentManager();
    }

    /**
     * If the operation is shown in the list of operations, return true
     */
    public boolean getShown(Issue issue, JiraAuthenticationContext authcontext) {
        log.debug("Checking that the operation is shown");

        if (targetProject.equalsIgnoreCase(CHOOSE) || targetIssueType.equalsIgnoreCase(CHOOSE)) {
            // Whether they choose the link type or it is prespecified
            // doesn't change whether the operation is shown
            return true;
        }

        String projectId = issue.getProjectObject().getId().toString();
        if (allowedProjects != null && !allowedProjects.isEmpty() && !allowedProjects.contains(projectId)) {
            log.debug("Project id " + projectId + " is not in the list of allowed projects");
            return false;
        }

        String issueType = issue.getIssueTypeObject().getId();
        if (allowedIssueTypes != null && !allowedIssueTypes.isEmpty() && !allowedIssueTypes.contains(issueType)) {
            log.debug("Issue type id " + issueType + " is not in the list of allowed issueTypes");
            return false;
        }

        // Check user has permission to create issues in the target project
        User user = authcontext.getUser();
        String tProject = targetProject;
        if (tProject.equalsIgnoreCase(INHERIT)) {
            tProject = projectId;
        }
        Project project = ComponentManager.getInstance().getProjectManager().getProjectObj(new Long(tProject));
        if (!ComponentManager.getInstance().getPermissionManager().hasPermission(Permissions.CREATE_ISSUE, project, user, true)) {
            log.debug("User " + user + " does not have permission to create an issue in " + project.getKey());
            return false;
        }

        log.debug("Operation is shown");
        return true;
    }

    /**
     * Check that this operation is still valid.  This could change
     * when the list of issue types, link types and projects changes.
     */
    boolean isValid() {
        if (!targetProject.equalsIgnoreCase(CHOOSE) && !targetProject.equalsIgnoreCase(INHERIT)) {
            if (ComponentManager.getInstance().getProjectManager().getProjectObj(new Long(targetProject)) == null) {
                log.warn("Create and Link operation '" + operationText + "' (" + id + ") is no longer valid because there is no project with id " + targetProject);
                return false;
            }
        }

        if (!targetIssueType.equalsIgnoreCase(CHOOSE) && !targetIssueType.equalsIgnoreCase(INHERIT)) {
            Collection<IssueType> issueTypes =  ComponentManager.getInstance().getConstantsManager().getRegularIssueTypeObjects();
            boolean found = false;
            for (IssueType issueType: issueTypes) {
                if (issueType.getId().equals(targetIssueType)) {
                    found = true;
                }
            }
            if (!found) {
                log.warn("Create and Link operation '" + operationText + "' (" + id + ") is no longer valid because there is no issue type now with id " + targetIssueType);
                return false;
            }
        }

        if (!linkType.equalsIgnoreCase(CHOOSE)) {
            Collection<IssueLinkType> linkTypes = issueLinkTypeManager.getIssueLinkTypes();
            boolean found = false;
            for (IssueLinkType itl: linkTypes) {
                if (itl.getId().toString().equals(linkType)) {
                    found = true;
                }
            }
            if (!found) {
                log.warn("Create and Link operation '" + operationText + "' (" + id + ") is no longer valid because there is no issue link type now with id " + linkType);
                return false;
            }
        }

        return true;
    }

    /**
     * Does the project of the new issue differ from the parent issue?
     */
    protected boolean didProjectChange(Issue issue) {
        if (targetProject.equalsIgnoreCase(INHERIT)) {
            return false;
        } else if (targetProject.equalsIgnoreCase(CHOOSE)) {
            // TODO how to check the chosen project here?
            return true;
        }
        Long src_id = issue.getProjectObject().getId();
        Long tgt_id = new Long(targetProject);
        return !src_id.equals(tgt_id);
    }

    /**
     * Create the HTML input elements for the HTTP POST action for the optional
     * parameters. This allows us to set or copy parameters from the
     * parent issue.
     */
    public String getOtherParameters(Issue issue, 
                                     JiraAuthenticationContext authcontext) {
        StringBuffer sb = new StringBuffer();

        Map argsMap = null;
        if (arguments != null && !arguments.equals("")) {
            // TODO pass the raw arguments param as well?
            //sb.append("<input type=\"hidden\" name=\"arguments\" value=\"" + arguments + "\">\n");

            // Parse arguments and use it to set parameters to pass in.
            // These values are writeable on the Create Screen.
            argsMap = parseArgs(arguments);
            if (argsMap == null) {
                log.error("Unable to parse arguments:\n" + arguments);
            }

            boolean didProjectChange = didProjectChange(issue);
            Iterator it = argsMap.entrySet().iterator();
            while(it.hasNext()) {
                Entry entry = (Entry) it.next();
                String fieldname = (String) entry.getKey();
                String[] fieldvalues = (String[]) entry.getValue();
                for (int i=0; i<fieldvalues.length; i++) {
                    String hiddenHtml = getHiddenHtml(issue, authcontext, fieldname, fieldvalues[i], didProjectChange);
                    log.debug("Hidden html for " + fieldname + "=" + fieldvalues[i] + " is: " + hiddenHtml);
                    if (hiddenHtml != null) {
                        
                        sb.append(hiddenHtml);
                    } else {
                        log.debug("Not adding hidden html for " + fieldname + " because it is null");
                    }
                }
            }
        }

        // TODO check for other required parameters in case they
        // weren't overridden

        // Assignee has to be set to something or -1 for Automatic
        if (argsMap != null) {
            if (!argsMap.containsKey("assignee")) {
                // Could use this? String assignee = authcontext.getUser().getName();
                sb.append("<input type=\"hidden\" name=\"assignee\" value=\"-1\">");
            }
        } else {
            // No arguments were defined
            sb.append("<input type=\"hidden\" name=\"assignee\" value=\"-1\">");
        }

        log.debug("Hidden parameters:\n" + sb.toString());
        return sb.toString();
    }

    /**
     *
     */
    protected Map parseArgs(String data) {
        // Has to support multiple keys for multivalued fields
        // TODO parse this once and cache it since it is called every time an issue is viewed
        Map m = new HashMap();
        String[] lines = data.split("\\r?\\n");
        for (int i=0; i<lines.length; i++) {
            String line = lines[i].trim();
            log.debug("Parsing line: " + line);
            String[] parts = line.split("=");
            if (parts.length != 2) {
                log.error("Unable to parse line: " + line);
                return null;
            }
            String fieldname = parts[0].trim();
            String fieldvalue = parts[1].trim();
            String[] existing = (String[])m.get(fieldname);
            String[] fieldvalues;
            int len = 0;
            if (existing == null) {
                fieldvalues = new String[len+1];
            } else {
                len = existing.length;
                fieldvalues = new String[len+1];
                for (int j=0; j<len; j++) {
                    log.debug("Value " + j + "=" + existing[j]);
                    fieldvalues[j] = existing[j];
                }
            }
            log.debug("Setting " + fieldname + "[" + len + "]=" + fieldvalue);
            fieldvalues[len] = fieldvalue;
            m.put(fieldname, fieldvalues);
        }
        return m;
    }

    /**
     * Multivalue fields get passed as multiple hidden fields
     * with the same name. Return null if no values are set.
     */
    protected String getMultipleValues(String name, Collection values, Issue issue, boolean didProjectChange) {
        String result = null;
        boolean isFirst = true;
        Iterator it = values.iterator();
        while(it.hasNext()) {
            Object object = it.next();
            GenericValue gv = null;
            if (object instanceof GenericValue) {
                gv = (GenericValue) object;
            } else if (object instanceof OfBizValueWrapper) {
                gv = ((OfBizValueWrapper) object).getGenericValue();
            } else {
                log.error("Unknown type: " + object.getClass().getName());
            }
            if (gv != null) {
                if (isFirst) {
                    isFirst = false;
                    result = "";
                } else {
                    result += "\n";
                }
                Long id = gv.getLong("id");
                if (didProjectChange) {
                    id = matchByName(name, gv.getString("name"), id, issue);
                }
                result += "<input type=\"hidden\" name=\"" + name + "\" value=\"" + id + "\">\n";
            }
        }
        return result;
    }

    /**
     * If the target project is not the same as the source project,
     * try to find a component or version with the same name and use
     * it. The generic value ids are unique across all projects
     * however. JCLP-30
     */
    private Long matchByName(String fieldName, String gvname, Long id, Issue issue) {
        if (targetProject.equalsIgnoreCase(CHOOSE)) {
            log.info("Component and version values are not mapped yet for Choose project in Create and Link operations"); // TODO
            return id;
        }

        Long tgt_id = new Long(targetProject);
        // Find a component or version in the new project with the given name
        if (fieldName.equals("versions") || fieldName.equals("fixVersions")) {
            Version v = versionManager.getVersion(tgt_id, gvname);
            if (v != null) {
                log.debug("Changing id for version to match the name " + gvname);
                return v.getId();
            } else {
                log.debug("matchByName: no version " + gvname + " found in the new project");
            }
        } else if (fieldName.equals("components")) {
            ProjectComponent c = projectComponentManager.findByComponentName(tgt_id, gvname);
            if (c != null) {
                log.debug("Changing id for component to match the name " + gvname);
                return c.getId();
            } else {
                log.debug("matchByName: no component " + gvname + " found in the new project");
            }
        } else {
            log.error("matchName was called with an unexpected fieldName: " + fieldName);
        }
        return id;
    }

    /**
     * Return the hidden value HTML for inherited custom fields, null
     * if nothing is set. Note that if the custom field is not valid
     * in the target project, nothing will get set.
     */
    protected String inheritCustomValues(String name, Issue issue) {
        String result = "";
        CustomField cf = ComponentManager.getInstance().getCustomFieldManager().getCustomFieldObject(name);
        if (cf == null) {
            log.warn("No custom field " + name + " was found");
            return null;
        }
        CustomFieldType cft = cf.getCustomFieldType();
        Object value = cft.getValueFromIssue(cf, issue);
        if (value == null) {
            return null;
        }
        log.debug("Found " + cft.getName() + " and " + cf.getName());

        // TODO does this handle all cases?
        if (value instanceof Collection) {
            Iterator it = ((Collection)value).iterator();
            while (it.hasNext()) {
                Object val = it.next();
                log.debug("Collection " + val);
                result += "<input type=\"hidden\" name=\"" + name + "\" value=\"" + val + "\">\n";
            }
        } else if (value instanceof Map) {
            Iterator it = ((Map)value).values().iterator();
            while (it.hasNext()) {
                Object val = it.next();
                log.debug("Map " + val);
                result += "<input type=\"hidden\" name=\"" + name + "\" value=\"" + val + "\">\n";
            }
        } else if (value instanceof CustomFieldParams && cft instanceof CascadingSelectCFType) {
            log.debug("Cascading Select " + value);
            CustomFieldParams params = (CustomFieldParams) value;
            Option parent = (Option) params.getValuesForKey(CascadingSelectCFType.PARENT_KEY).iterator().next();
            result += "<input type=\"hidden\" name=\"" + name + "\" value=\"" + parent.getOptionId() + "\">\n";
            Option child = (Option) params.getValuesForKey(CascadingSelectCFType.CHILD_KEY).iterator().next();
            result += "<input type=\"hidden\" name=\"" + name + ":1\" value=\"" + child.getOptionId() + "\">\n";
        } else {
            log.debug("Other " + value.getClass().getName());
            result += "<input type=\"hidden\" name=\"" + name + "\" value=\"" + value + "\">\n";
        }
        return result;
    }

    /**
     * Extract the field value from the parent issue.
     *
     * Note: these values are snapshots of when the View Issue screen
     * was loaded.
     */
    protected String getHiddenHtml(Issue issue, JiraAuthenticationContext authcontext, String key, String value, boolean didProjectChange) {
        String result = null;
        String valueStr = null;
        if (value.equalsIgnoreCase(INHERIT)) {
            if (key.equals("assignee")) {
            	User assignee = issue.getAssignee();
            	valueStr = (assignee != null) ? assignee.getName() : "";
            } else if (key.equals("components")) {
                valueStr = null;
                result = getMultipleValues("components", issue.getComponents(), issue, didProjectChange);
            } else if (key.equals("description")) {
                valueStr = issue.getDescription();
            } else if (key.equals("duedate")) {
                valueStr = "";
                Timestamp ts = issue.getDueDate();
                if (ts != null) {
                    // Convert to required format such as 20/Jan/11
                    valueStr = outlookDateManager.getOutlookDate(authcontext.getLocale()).formatDatePicker(ts);
                }
            } else if (key.equals("environment")) {
                valueStr = issue.getEnvironment();
            } else if (key.equals("fixVersions")) {
                valueStr = null;
                result = getMultipleValues("fixVersions", issue.getFixVersions(), issue, didProjectChange);
            } else if (key.equals("priority")) {
                Priority priority = issue.getPriorityObject();
                if (priority != null) {
                    valueStr = priority.getId();
                }
            } else if (key.equals("resolution")) {
                Resolution resolution = issue.getResolutionObject();
                if (resolution != null) {
                    valueStr = resolution.getId();
                }
            } else if (key.equals("security")) {
                Long secId = issue.getSecurityLevelId();
                valueStr = null;
                if (secId != null) {
                    valueStr = secId.toString();
                }
            } else if (key.equals("summary")) {
                valueStr = issue.getSummary();
            } else if (key.equals("versions")) {
                valueStr = null;
                result = getMultipleValues("versions", issue.getAffectedVersions(), issue, didProjectChange);
            } else if (key.startsWith("customfield_")) {
                valueStr = null;
                result = inheritCustomValues(key, issue);
            } else {
                log.error("Unimplemented field name key in arguments: " + key);
                // votes and watchers could make sense
                // and labels and time tracking too?
                return null;
            }

            if (valueStr != null) {
                String name = key; // assumed
                result = "<input type=\"hidden\" name=\"" + name + "\" value=\"" + StringEscapeUtils.escapeHtml(valueStr) + "\">\n";
            }
        } else if (value.indexOf(INHERIT_REPLACEMENT) != -1) {
            // For certain fields replace the substring with the inherited value
            valueStr = value; // the raw text with the inherit keyword substring
            String v = null;
            if (key.equals("description")) {
                v = issue.getDescription();
            } else if (key.equals("environment")) {
                v = issue.getEnvironment();
            } else if (key.equals("summary")) {
                v = issue.getSummary();
            }
            if (v != null) {
                valueStr = valueStr.replaceAll(INHERIT_REPLACEMENT, v);
            }
            if (valueStr != null) {
                String name = key; // assumed
                result = "<input type=\"hidden\" name=\"" + name + "\" value=\"" + StringEscapeUtils.escapeHtml(valueStr) + "\">\n";
            }

       // } else if (value.equalsIgnoreCase(MODIFY)) { // Could provide mappers here
        } else {
            // Just set the field to the given value
            // Assumes that the names of the keys are the same as the fieldnames used by JIRA and that users have specified the correct id or option value.
            result = "<input type=\"hidden\" name=\"" + key + "\" value=\"" + StringEscapeUtils.escapeHtml(value) + "\">\n";
        }
        
        return result;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setArguments(String value) {
        arguments = value;
        // Remove trailing newlines
        while (arguments.endsWith("\n") || arguments.endsWith("\r")) {
            arguments = arguments.substring(0, arguments.length()-1);
        }
    }

    public String getId() {
        return id;
    }

    public String getOperationText() {
        return operationText;
    }

    public void setOperationText(String value) {
        this.operationText = value;
    }

    public String getTargetProject() {
        return targetProject;
    }

    public void setTargetProject(String value) {
        this.targetProject = value;
    }

    public String getTargetProjectName() {
        if (targetProject.equalsIgnoreCase(INHERIT)) {
            return INHERIT_FROM_ISSUE;
        } else if (targetProject.equalsIgnoreCase(CHOOSE)) {
            return CHOOSE_WHEN_USED;
        } else {
            return ComponentManager.getInstance().getProjectManager().getProjectObj(new Long(targetProject)).getName();
        }
    }

    public String getTargetIssueType() {
        return targetIssueType;
    }

    public void setTargetIssueType(String value) {
        this.targetIssueType = value;
    }

    public String getTargetIssueTypeName() {
        if (targetIssueType.equalsIgnoreCase(INHERIT)) {
            return INHERIT_FROM_ISSUE;
        } else if (targetIssueType.equalsIgnoreCase(CHOOSE)) {
            return CHOOSE_WHEN_USED;
        } else {
            return ComponentManager.getInstance().getConstantsManager().getIssueTypeObject(targetIssueType).getName();
        }
    }

    public String getLinkType() {
        return linkType;
    }

    public void setLinkType(String value) {
        this.linkType = value;
    }

    public String getLinkDirection() {
        if (linkInward) {
            return "inward";
        } else {
            return "outward";
        }
    }

    public void setLinkDirection(boolean value) {
        this.linkInward = value;
    }

    /**
     * Used to display the current link as "link name - direction text"
     */
    public String getLinkTypeName() {
        if (linkType.equalsIgnoreCase(CHOOSE)) {
            return CHOOSE_WHEN_USED;
        }
        IssueLinkType itl = issueLinkTypeManager.getIssueLinkType(new Long(linkType));
        StringBuffer sb = new StringBuffer(itl.getName());
        sb.append(" - ");
        if (linkInward) {
            sb.append(itl.getInward());
        } else {
            sb.append(itl.getOutward());
        }
        return sb.toString();
    }

    public String getArguments() {
        return arguments;
    }

    /**
     * Covert new lines to HTML br elements
     */
    public String getArgumentsHtml() {
        return arguments.replaceAll("\\r?\\n", "<br>");
    }

    public void setAllowedProjects(Collection allowedProjects) {
        this.allowedProjects = allowedProjects;
    }

    public void setAllowedIssueTypes(Collection allowedIssueTypes) {
        this.allowedIssueTypes = allowedIssueTypes;
    }

    // Methods used to get the parameter values to pass to the posted form

    public String getParamActionName() {

        if (targetProject.equalsIgnoreCase(CHOOSE) || 
            targetIssueType.equalsIgnoreCase(CHOOSE) ||
            linkType.equalsIgnoreCase(CHOOSE)) {
            return "CreateAndLink!PreCreateAndLink.jspa";
        } else {
            return "CreateAndLink!init.jspa";
        }
    }

    public String getParamPid(Issue issue) {
        // If it was CHOOSE, getParamActionName would handle it
        if (targetProject.equalsIgnoreCase(INHERIT)) {
            return issue.getProjectObject().getId().toString();
        } else {
            return targetProject;
        }
    }

    public String getParamIssueType(Issue issue) {
        // If it was CHOOSE, getParamActionName would handle it
        if (targetIssueType.equalsIgnoreCase(INHERIT)) {
            return issue.getIssueTypeObject().getId();
        } else {
            return targetIssueType;
        }
    }

    public String getParamLinkType(Issue issue) {
        // If it was CHOOSE, getParamActionName would handle it, and link
        // types aren't inherited
        return linkType;
    }

    public String getParamLinkDirection(Issue issue) {
        if (linkInward) {
            return "inward";
        } else {
            return "outward";
        }
    }

    public String getParamText(Issue issue) {
        return operationText;
    }

    /**
     * @return the reporter for the new issue, which is the current user not the reporter of the original issue
     */
    public String getParamReporter(Issue issue, 
                                   JiraAuthenticationContext authcontext) {
        User user = authcontext.getUser();
        // Checking that the user has permission to create issues in
        // the target project is done in getShown()
        return user.getName();
        // JCLP-19 was issue.getReporter().getName();
    }


    /**
     * Accessor for the singleton issueLinkTypeManager
     */
    protected static IssueLinkTypeManager getIssueLinkTypeManager() {
        if (issueLinkTypeManager == null) {
            issueLinkTypeManager = (IssueLinkTypeManager) ComponentManager.getComponentInstanceOfType(IssueLinkTypeManager.class);
        }
        return issueLinkTypeManager;
    }
    
    private static IssueLinkTypeManager issueLinkTypeManager;

    // Methods and a constructor for the Persistable interface

    /**
     * Create an XML representation of this object and is match by the
     * reverse process in the contructor.
     */
    public Element getXML(Document doc) {
        // The child element have to have the same name as the class name
        // for Persister.stringToMap() to find them
        Element e = doc.createElement(this.getClass().getName().replace('.', '_'));
        // These appear in alphabetical order in the String of the XML
        e.setAttribute("id", getId());
        e.setAttribute("operationtext", getOperationText());
        e.setAttribute("targetproject", getTargetProject());
        e.setAttribute("targetissuetype", getTargetIssueType());
        e.setAttribute("linktype", getLinkType());
        e.setAttribute("linkdirection", getLinkDirection());
        e.setAttribute("arguments", getArguments());
        
        //e.setAttribute("allowedprojects", "TODO");
        //e.setAttribute("allowedissuetypes", "TODO");
        return e;
    }

    /**
     * The constructor used when recreating instances of this class from their
     * XML representation.
     */
    public Operation(String dataVersion, Node node) {
        NamedNodeMap attrs = node.getAttributes();
        // Required values
        this.id = attrs.getNamedItem("id").getNodeValue();
        // getKey is valid after this
        // TODO check that these are all still valid values
        // and log.warn and throw an exception if not
        this.targetProject = attrs.getNamedItem("targetproject").getNodeValue();
        this.targetIssueType = attrs.getNamedItem("targetissuetype").getNodeValue();
        this.linkType = attrs.getNamedItem("linktype").getNodeValue();
        this.operationText = attrs.getNamedItem("operationtext").getNodeValue();

        // Optional values
        Node argNode = attrs.getNamedItem("arguments");
        if (argNode != null) {
            // arguments maybe null
            this.arguments = argNode.getNodeValue();
        }

        if (!dataVersion.equals("1.0")) {
            String linkDirection = attrs.getNamedItem("linkdirection").getNodeValue();
            this.linkInward = linkDirection.equalsIgnoreCase("inward");
        } else {
            this.linkInward = false;
        }

        issueLinkTypeManager = getIssueLinkTypeManager();

        this.outlookDateManager = ManagerFactory.getOutlookDateManager();
        this.versionManager = ComponentManager.getInstance().getVersionManager();
        this.projectComponentManager = ComponentManager.getInstance().getProjectComponentManager();
    }

    public Object getKey() {
        return getId();
    }
    
    
    /**
     *  A comparator for keeping operations sorted by their .
     */
    public static class OperationComp implements Comparator {
        
        public int compare(Object a, Object b) {
            Operation opA = (Operation) a;
            Operation opB = (Operation) b;
            String textA = opA.getOperationText();
            String textB = opB.getOperationText();
            return textA.compareToIgnoreCase(textB);
        }
    }
    
}
