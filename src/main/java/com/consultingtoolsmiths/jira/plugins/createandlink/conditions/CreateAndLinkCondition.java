package com.consultingtoolsmiths.jira.plugins.createandlink.conditions;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;
import com.consultingtoolsmiths.jira.plugins.createandlink.CreateAndLinkAdminAction;
import com.consultingtoolsmiths.jira.plugins.createandlink.Operation;
import com.opensymphony.user.User;

/*
 * In order to "Create and Link", issue linking must be enabled, and the user must have permission through at least one operation
 * to create issues in a target project and link issues in the current project.
 */
public class CreateAndLinkCondition implements Condition {

    private static final Log LOG = LogFactory.getLog(CreateAndLinkCondition.class);

    private final PermissionManager permissionManager;
    private final IssueLinkManager issueLinkManager;
    private final ProjectManager projectManager;

    public CreateAndLinkCondition(final PermissionManager permissionManager, final IssueLinkManager issueLinkManager,
            final ProjectManager projectManager) {
        this.permissionManager = permissionManager;
        this.issueLinkManager = issueLinkManager;
        this.projectManager = projectManager;
    }

    public void init(Map<String, String> params) throws PluginParseException {
        LOG.info("Initializing.");
    }

    public boolean shouldDisplay(Map<String, Object> context) {
        Project currentProject = (Project) context.get("project");
        Issue currentIssue = (Issue) context.get("issue");
        User currentUser = (User) context.get("user");
        boolean display = false;
        if (!this.issueLinkManager.isLinkingEnabled()) {
            LOG.warn("Issue linking is not enabled!");
        } else if (currentProject == null) {
            LOG.warn("The current project is null!");
        } else if (currentIssue == null) {
            LOG.warn("The current issue is null!");
        } else if (currentUser == null) {
            LOG.warn("The current user is null!");
        } else {
            /*
             * Make sure we can link issues in the current project
             */
            if (this.permissionManager.hasPermission(Permissions.LINK_ISSUE, currentProject, currentUser)) {
                Collection<Operation> operations = CreateAndLinkAdminAction.getAllOperations();
                /*
                 * See if we can create issues in the target project(s). If so
                 * we can display the action, otherwise check the next
                 * operation.
                 */
                for (Operation operation : operations) {
                    String tp = operation.getTargetProject();
                    if (Operation.INHERIT.equalsIgnoreCase(tp)) {
                        if (this.permissionManager.hasPermission(Permissions.CREATE_ISSUE, currentProject, currentUser)) {
                            display = true;
                        }
                    } else if (Operation.CHOOSE.equalsIgnoreCase(tp)) {
                        /*
                         * Get all the projects in which the current user can
                         * create issues
                         */
                        if (!this.permissionManager.getProjectObjects(Permissions.CREATE_ISSUE, currentUser).isEmpty()) {
                            display = true;
                        }
                    } else {
                        Project project = this.projectManager.getProjectObj(Long.valueOf(tp));
                        if (project != null
                                && this.permissionManager.hasPermission(Permissions.CREATE_ISSUE, project, currentUser)) {
                            display = true;
                        }
                    }
                    if (display) {
                        break;
                    }
                }
            } else {
                LOG.warn("User " + currentUser.getName() + " does not have permission to link in the current project "
                        + currentProject.getKey());
            }
        }
        return display;
    }

}
