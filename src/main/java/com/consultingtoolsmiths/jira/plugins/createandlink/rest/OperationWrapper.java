package com.consultingtoolsmiths.jira.plugins.createandlink.rest;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.consultingtoolsmiths.jira.plugins.createandlink.Operation;

/**
 * JAXB version of the Operation class, used for REST transfer.
 */
@XmlRootElement(name = "operation")
public class OperationWrapper {
    
    @XmlElement(name = "text")
    private String text;

    @XmlElement(name = "actionName")
    private String actionName;

    @XmlElement(name = "id")
    private String id;

    @XmlElement(name = "reporter")
    private String reporter;
    
    @XmlElement(name = "pid")
    private String pid;
    
    @XmlElement(name = "issuetype")
    private String issueType;

    @XmlElement(name = "linktype")
    private String linkType;

    @XmlElement(name = "linkdirection")
    private String linkDirection;

    @XmlElement(name = "opid")
    private String opid;

    @XmlElement(name = "otherParams")
    private String otherParams;

    @XmlElement(name = "shown")
    private boolean shown;

    public OperationWrapper() {}

    /**
     *
     * @param operation the Operation to be used by the created link
     * @param issue the parent issue to be linked to
     * @param jiraAuthenticationContext the current authentication details
     */
    public OperationWrapper(Operation operation,
                            Issue issue,
                            JiraAuthenticationContext jiraAuthenticationContext) {
        this.text = operation.getParamText(issue);
        this.actionName = operation.getParamActionName();
        this.id = issue.getId().toString();
        this.reporter = operation.getParamReporter(issue,
                                                   jiraAuthenticationContext);
        this.pid = operation.getParamPid(issue);
        this.issueType = operation.getParamIssueType(issue);
        this.linkType = operation.getParamLinkType(issue);
        this.linkDirection = operation.getParamLinkDirection(issue);
        this.opid = operation.getId();
        this.shown = operation.getShown(issue, jiraAuthenticationContext);
        this.otherParams = operation.getOtherParameters(issue,
                                                        jiraAuthenticationContext);
    }
    
    public String getText() {
        return this.text;
    }
    
    public String getActionName() {
        return this.actionName;
    }
    
    public String getId() {
        return this.id;
    }
    
    public String getReporter() {
        return this.reporter;
    }
    
    public String getPid() {
        return this.pid;
    }
    
    public String getIssueType() {
        return this.issueType;
    }
    
    public String getLinkType() {
        return this.linkType;
    }
    
    public String getLinkDirection() {
        return this.linkDirection;
    }
    
    public String getOpid() {
        return this.opid;
    }
    
    public boolean isShown() {
        return this.shown;
    }
    
    public String getOtherParams() {
        return this.otherParams;
    }
}
