package com.atlassian.jira.web.action.custom;

import java.io.PrintWriter;
import com.atlassian.jira.util.EasyList;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayout;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.screen.FieldScreenRendererFactory;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.util.ImportUtils;
import com.atlassian.jira.web.action.issue.CreateIssueDetails;
import com.atlassian.jira.web.action.issue.IssueCreationHelperBean;
import com.atlassian.jira.web.bean.PermissionCheckBean;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.consultingtoolsmiths.jira.plugins.createandlink.*;
import java.util.*;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.GenericValue;
import webwork.action.ActionContext;

/**
 * The class that actually creates the issue and links it.
 * Based on CreateIssueDetails around JIRA 4.0
 *
 * Note: the validation changes in 4.1 mean that much of this is using
 * code that will be deprecated
 */
public class CreateAndLink extends CreateIssueDetails
{
    protected final ApplicationProperties applicationProperties;
    protected final PermissionManager permissionManager;
    protected final IssueLinkManager issueLinkManager;
    protected final IssueLinkTypeManager issueLinkTypeManager;
    protected final SubTaskManager subTaskManager;
    protected final FieldManager fieldManager;
    protected final IssueFactory issueFactory;
    protected final IssueManager issueManager;
    protected final IssueIndexManager issueIndexManager;
    protected final IssueTypeSchemeManager issueTypeSchemeManager;
    protected final FieldScreenRendererFactory fieldScreenRendererFactory;
    protected final PermissionCheckBean permissionCheck;
    protected final StringUtils stringUtils;
    protected final WebResourceManager webResourceManager;

    // The original issue that is to be linked to
    private Issue originalIssue;

    // The type of link
    private IssueLinkType linkType;
    private String linkDirection;

    public CreateAndLink(ApplicationProperties applicationProperties, PermissionManager permissionManager,
                             IssueLinkManager issueLinkManager, IssueLinkTypeManager issueLinkTypeManager, SubTaskManager subTaskManager,
                         FieldManager fieldManager, IssueCreationHelperBean issueCreationHelperBean, IssueFactory issueFactory, IssueManager issueManager, IssueIndexManager issueIndexManager,
                         IssueTypeSchemeManager issueTypeSchemeManager,
                         FieldScreenRendererFactory fieldScreenRendererFactory,
                         IssueService issueService,
                         JiraAuthenticationContext jiraAuthenticationContext,
                         WebResourceManager webResourceManager
                         )
    {
        super(issueFactory, issueCreationHelperBean, issueService);
        this.applicationProperties = applicationProperties;
        this.permissionManager = permissionManager;
        this.issueLinkManager = issueLinkManager;
        this.issueLinkTypeManager = issueLinkTypeManager;
        this.subTaskManager = subTaskManager;
        this.fieldManager = fieldManager;
        this.issueFactory = issueFactory;
        this.issueManager = issueManager;
        this.issueIndexManager = issueIndexManager;
        this.issueTypeSchemeManager = issueTypeSchemeManager;
        this.fieldScreenRendererFactory = fieldScreenRendererFactory;
        this.permissionCheck = new PermissionCheckBean(jiraAuthenticationContext, permissionManager);
        this.stringUtils = new StringUtils();
        this.webResourceManager = webResourceManager;
    }

    /**
     * doInit sets up the context for the page that is displayed.
     */
    public String doInit()
    {
        log.debug("Entering doInit");

        /* This is no longer needed as cancel is a link back to the return url -JMP
        String isCancel = request.getParameter("Cancel");
        if (isCancel != null) {
            String cancelUrl = request.getParameter("cancelUrl");
            if (cancelUrl != null) {
                log.debug("Cancel received with URL " + cancelUrl);
                return forceRedirect(cancelUrl);
            } 
            return ERROR;
        }
        */

        if (!isAbleToCreateIssueInSelectedProject()) {
            return ERROR;
        }

        SuperSuperValidation();
        if (invalidInput()) {
            return ERROR;
        }
        
        populateFieldHolder(getIssueObject(),
                            Collections.EMPTY_LIST,
                            ActionContext.getParameters());
        if (invalidInput()) {
            return ERROR;
        }

        return INPUT;
    }
    
    /**
     * Check the project and issuetype are a valid combination.
     * From CreateIssue doValidation.
     */
    protected void SuperSuperValidation()
    {
        try
        {
            issueCreationHelperBean.validateProject(getIssueObject(), this, ActionContext.getParameters(), this, this);
            if (!invalidInput())
            {
                getIssueObject().setProject(getProject());
            }

            issueCreationHelperBean.validateIssueType(getIssueObject(), this, ActionContext.getParameters(), this, this);
            if (!invalidInput())
            {
                getIssueObject().setIssueTypeId(getIssuetype());
            }

            // TODO also validate the link type
        }
        catch (Exception e)
        {
            log.error(e, e);
            addErrorMessage("An exception occurred: " + e + ".");
        }
    }

    /**
     * Populate all fields, whether shown or not, with the values in
     * actionParams or their defaults if none are specified in
     * actionParams.
     */
    protected void populateFieldHolder(Issue issue, Collection excludedFieldIds, Map actionParams)
    {
        FieldLayout fieldLayout = getFieldScreenRenderer().getFieldLayout();
        for (Iterator it = fieldLayout.getFieldLayoutItems().iterator(); it.hasNext(); ) {
            FieldLayoutItem fieldLayoutItem = (FieldLayoutItem) it.next();
            if (fieldLayoutItem == null) {
                continue;
            }
            OrderableField orderableField = fieldLayoutItem.getOrderableField();
            if (orderableField == null) {
                continue;
            }
            String fieldId = orderableField.getId();
            if (excludedFieldIds.contains(fieldId) || 
                IssueFieldConstants.PROJECT.equals(fieldId) || 
                IssueFieldConstants.ISSUE_TYPE.equals(fieldId) ||
                IssueFieldConstants.RESOLUTION.equals(fieldId)) {
                continue;
            }
            if (isFieldSpecifiedInMap(fieldId, actionParams)) {
                log.debug("Populating field " + orderableField + " from action parameters: " + actionParams.get(fieldId));
                orderableField.populateFromParams(getFieldValuesHolder(), actionParams);
            } else {
                if (getFieldValuesHolder().get(fieldId) == null) {
                    // Some fields seem to already be present in the
                    // fieldHolder with defaults
                    log.debug("Setting default value for field " + orderableField);
                    orderableField.populateDefaults(getFieldValuesHolder(), issue);
                } else {
                    log.debug("Field " + orderableField + " is already set");
                }
            }
        }
        
        /*
         * Check that all the non-visible required fields have values.
         * 
         */
        List<FieldLayoutItem> requiredItems = fieldLayout.getRequiredFieldLayoutItems(getRemoteUser(), issue.getProject(), EasyList.build(issue.getIssueTypeObject().getId()));
        List visibleItems = fieldLayout.getVisibleLayoutItems(getRemoteUser(), issue.getProjectObject(), EasyList.build(issue.getIssueTypeObject().getId()));
        for (FieldLayoutItem fieldLayoutItem : requiredItems) {
            if (fieldLayoutItem == null) {
                continue;
            }
            if (visibleItems.contains(fieldLayoutItem)) {
                // If it is left blank, there will be a warning
                continue;
            }
            OrderableField orderableField = fieldLayoutItem.getOrderableField();
            if (orderableField == null) {
                continue;
            }
            //log.debug("Checking required field " + orderableField);
            String fieldId = orderableField.getId();

            Object currValue = getFieldValuesHolder().get(fieldId);
            //log.debug("Required field " + orderableField + " is set to " + currValue);
            // Some users won't have the ability to set the default
            // security level on an issue? No, that isn't checked when
            // setting the default for that field.
            if (currValue == null) {
                String msg = "The required field '" + orderableField.getName() + "' is not shown but its default value is null. For example, if Security Level is a required field, then the target project must have an issue security scheme with a valid default.";
                addErrorMessage(msg);
                log.error(msg);
            }
        }
    }

    // Only present because it is protected in IssueCreationHelperBean
    /**
     * If the key does exist in the map, this will attempt to determine if the value is "empty" using {@link #isObjectEmpty(Object)}
     *
     * @param fieldId the field to look up. In the case of custom fields, you needn't worry about specifying the "full key"; just customfield_xxxxx will do.
     * @param map a mapping from String to Object containing field values
     * @return true if the field was specified in the map AND had a value which was not empty
     */
    boolean isFieldSpecifiedInMap(final String fieldId, final Map/*<String, Object>*/ map)
    {
        for (Iterator iterator = map.keySet().iterator(); iterator.hasNext();)
        {
            Object o = iterator.next();
            if (o instanceof String)
            {
                String key = (String) o;
                // TODO JCLP-12 should this be equals?
                if (key.startsWith(fieldId))
                {
                    if (!isObjectEmpty(map.get(key)))
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    // Only present because it is protected in IssueCreationHelperBean
    /**
     * Checks for "emptiness" of the specified value. Knows about Arrays, {@link java.util.Collection}s and
     * {@link com.atlassian.jira.issue.customfields.view.CustomFieldParams}. Null values are empty.
     *
     * @param value the value to test
     * @return true if null or "empty"; false otherwise.
     */
    boolean isObjectEmpty(final Object value)
    {
        if (value == null)
        {
            return true;
        }

        // sometimes we encounter arrays with one null element - these are deemed empty
        if (value instanceof Object[])
        {
            final Object[] objects = (Object[]) value;
            if (objects.length == 0 || (objects.length == 1 && objects[0] == null))
            {
                return true;
            }
        }

        if (value instanceof Collection)
        {
            return ((Collection) value).isEmpty();
        }

        if (value instanceof CustomFieldParams)
        {
            return ((CustomFieldParams) value).isEmpty();
        }

        return false;
    }

    /**
     * doValidate is called when the form is submitted.
     *
     * Validate the parameters in the request. If an error message is set,
     * then doExecute is not called.
     */
    protected void doValidation() {
//         for (Iterator it = ActionContext.getParameters().entrySet().iterator(); it.hasNext(); ) {
//             Map.Entry entry = (Entry)it.next();
//             log.debug("GOT key " + entry.getKey() + ", value " + entry.getValue());
//         }

        super.doValidation();
        if (invalidInput())
        {
            for (Iterator iterator = getErrors().entrySet().iterator(); iterator.hasNext();)
            {
                Map.Entry entry = (Map.Entry) iterator.next();
                log.error("Error during issue details validation: " + entry.getKey() + ": " + entry.getValue());
            }
            return;
        }

        // The link type will have already been stored with setLinktype() so
        // that the form can reload it after an error using hidden variables

        String id = getId().toString();
        if (id == null || id.equals("")) {
            addErrorMessage("Unable to find the parameter 'id' for the parent issue id");
        } else {
            Long parentId = Long.valueOf(id);
            Issue parentIssue = issueManager.getIssueObject(parentId);
            if (parentIssue == null) {
                addErrorMessage("Unable to find a parent issue with id: " + parentId);
            }
        }
    }

    /**
     * This is the action method that is called by the link in the More Actions
     * menu and returns the screen based on pre_createandlink.vm with
     * the createandlink.js file included.
     */
    @RequiresXsrfCheck
    public String doPreCreateAndLink() throws Exception
    {
    	this.webResourceManager.requireResource("com.consultingtoolsmiths.jira.plugins.createandlink:createandlink_js");
        // Note that if an action method is protected instead of public, then
        // it appears that the action has no such command.
        log.debug("Entering doPreCreateAndLink");
        return "prescreen";
    }

    /**
     * Method used by an Ajax call to get the current set of issue types for
     * a chosen project. Output the issue type information for use by
     * a Javascript function.
     */
    @Deprecated // Moved to IssueTypesService
    @RequiresXsrfCheck
    public void doUpdate() throws Exception
    {
        String pidStr = request.getParameter("projectid");
        log.debug("Entering doUpdate with a project id of " + pidStr);
        if (pidStr == null) {
            log.warn("No project id parameter found");
            return;
        }
        Long pid;
        try {
            pid = Long.valueOf(pidStr);
        } catch (NumberFormatException nfe) {
            log.warn("Project id parameter " + pidStr + " is not an integer");
            return;
        }
        // These are not subtasks
        Collection issuetypes = getIssueTypesForProject(ComponentManager.getInstance().getProjectManager().getProjectObj(pid));
        log.debug("Returning " + issuetypes.size() + " issuetype(s)");
        PrintWriter out = ActionContext.getResponse().getWriter();
        // Return javascript text that can be eval'd. json might be easier?
        for (Iterator it = issuetypes.iterator(); it.hasNext(); ) {
            IssueType itype = (IssueType)it.next();
            log.debug("Issue type: " + itype.getName());
            out.write("itchoices.push([\"" +  itype.getName() + "\", \"" + itype.getId() + "\"]);\n");
        }
    }

    @RequiresXsrfCheck
    protected String doExecute() throws Exception
    {
        /*
        // Use get/set methods instead to access parameter values
        for (Enumeration e =  request.getParameterNames(); e.hasMoreElements() ;) {
            String n = (String)e.nextElement();
            String[] vals = request.getParameterValues(n);
            log.debug("name " + n + ": " + vals[0]);
        }
        */

        Long parentId = getId();
        Issue parentIssue = issueManager.getIssueObject(parentId);
        setOriginalIssue(parentIssue);

        return super.doExecute();
    }

    public MutableIssue getIssueObject(GenericValue genericValue)
    {
        return issueFactory.getIssue(genericValue);
    }

    /**
     * The issue has already been created by this time, so avoid failure here
     */
    protected String doPostCreationTasks() throws Exception
    {
        Issue originalIssue = getOriginalIssue();
        MutableIssue currentIssue = getIssueObject(getIssue());
        log.debug("Creating a link  of type " + linkType.getName() + " (direction: " + linkDirection + ") between " + originalIssue.getKey() + " and " + currentIssue.getKey());
        if (linkDirection == null || linkDirection.equals("inward")) {
            // Reversed two first parameters
            issueLinkManager.createIssueLink(currentIssue.getId(),
                                             originalIssue.getId(),
                                             linkType.getId(),
                                             null, // sequence
                                             getRemoteUser());
        } else {
            issueLinkManager.createIssueLink(originalIssue.getId(),
                                             currentIssue.getId(),
                                             linkType.getId(),
                                             null, // sequence
                                             getRemoteUser());
        }

        try {
            // reindex issue
            // TODO the 4.1 javadocs claim that createIssueLink does this
            // and now an error appears in the log:
            // Indexing thread local not cleared. Clearing...
            ImportUtils.setIndexIssues(true);
            issueIndexManager.reIndex(currentIssue);
            ImportUtils.setIndexIssues(false);
        } catch(IndexException e) {
            throw e;
        }

        // Return to the parent issue to make it easier to create
        // and link multiple issues
        // TODO should this be a user choice?
        if (permissionManager.hasPermission(Permissions.BROWSE, originalIssue, getRemoteUser()))
            return getRedirect("/browse/" + originalIssue.getKey() + "?atl_token=" + getXsrfToken());
        else
            return getRedirect("CantBrowseCreatedIssue.jspa?atl_token=" + getXsrfToken() + "&issueKey=" + originalIssue.getKey());  
    }

    /**
     * The list of fields that are not shown on a create issue screen.
     */
    public Collection getIgnoreFieldIds() {
        Collection fieldIds = super.getIgnoreFieldIds();
        // TODO This can be used to make fields harder to change by the user?
        // In fact if a field is not shown, it cannot be set at all so
        // that doesn't work.
        //fieldIds.add(IssueFieldConstants.ASSIGNEE);
        return fieldIds;
    }

    // ------ Getters & Setters & Helper Methods -----------------
    public StringUtils getStringUtils()
    {
    	return this.stringUtils;
    }
    
    public PermissionCheckBean getPermissionCheck()
    {
    	return this.permissionCheck;
    }

    public Issue getOriginalIssue()
    {
        return originalIssue;
    }

    public void setOriginalIssue(Issue originalIssue)
    {
        this.originalIssue = originalIssue;
    }

    // The id of the parent issue
    private Long id;

    public void setId(Long id) {
        log.debug("Setting id to " + id);
        this.id = id;
    }

    public Long getId() {
        log.debug("Getting id of " + id);
        return id;
    }

    // The extra arguments
    private String arguments;

    public void setArguments(String arguments) {
        log.debug("Setting arguments to " + arguments);
        this.arguments = arguments;
    }

    public String getArguments() {
        log.debug("Getting arguments of " + arguments);
        return arguments;
    }

    public Collection getAllowedProjects()
    {
        Collection allowedProjects = getPermissionManager().getProjects(Permissions.CREATE_ISSUE, getRemoteUser());
        String issuetype = getIssuetype();
        if (issuetype == null || issuetype.equalsIgnoreCase("choose")) {
            return allowedProjects;
        }
        // Avoid ConcurrentModificationException
        Collection result = new ArrayList(allowedProjects);
        // Find the projects that issuetype is valid for
        for (Iterator it = allowedProjects.iterator(); it.hasNext(); ) {
            GenericValue project = (GenericValue)it.next();
            Collection allowedIssueTypes = issueTypeSchemeManager.getIssueTypesForProject(project);
            boolean found = false;
            for (Iterator it2 = allowedIssueTypes.iterator(); it2.hasNext(); ) {
                IssueType itype = (IssueType)it2.next();
                if (itype.getId().equals(issuetype)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                log.debug("Issue type " + issuetype + " is not in the issue types for the project " + project.getString("key"));
                result.remove(project);
            }
        }
        return result;
    }

    public String getLinkdirection() {
        log.debug("Getting link direction of " + linkDirection);
        return linkDirection;
    }

    public void setLinkdirection(String value) {
        log.debug("Setting link direction to " + value);
        // TODO check "outward" or "inward"
        linkDirection = value;
    }

    public String getLinktype()
    {
        if (linkType == null) {
            return "";
        }
        log.debug("Getting the link type of " + linkType.getName());
        return linkType.getId().toString();
    }

    public String getLinktypename()
    {
        return linkType.getName();
    }

    public void setLinktype(String value)
    {
        log.debug("Setting link type id from " + value);
        String linkTypeId = value;
        if (linkTypeId.indexOf('_') != -1) {
            String[] vals = linkTypeId.split("_");
            linkTypeId = vals[0];
            setLinkdirection(vals[1]);
        }
        linkType = issueLinkTypeManager.getIssueLinkType(new Long(linkTypeId));
        if (linkType == null && !linkTypeId.equalsIgnoreCase("choose")) {
            String msg = "Unable to find a link type with id: " + linkTypeId;
            addErrorMessage(msg);
            log.warn(msg);
        }
    }

    public Collection getIssueTypes() {
        Long pid = getPid();
        if (pid != null) {
            // These not subtasks
            return getIssueTypesForProject(ComponentManager.getInstance().getProjectManager().getProjectObj(pid));
        }
        return ComponentManager.getInstance().getConstantsManager().getRegularIssueTypeObjects();
    }

    public Collection getLinktypes() {
        return issueLinkTypeManager.getIssueLinkTypes();
    }

    // To avoid null comparisons in a .vm file
    public String getNonNullPid() {
        Long pid = getPid();
        if (pid != null) {
            return pid.toString();
        }
        return "";
    }

    // Used for repassing the same hidden parameters in pre_createandlink.vm
    // TODO there must be a better way, perhaps using javascript to
    // repopulate the DOM?
    
    // The id of the current operation
    private String opid;

    public String getOpid() {
        return opid;
    }
    public void setOpid(String value) {
        this.opid = value;
    }

    public String getOtherParameters(GenericValue issue, 
                                     JiraAuthenticationContext authcontext) {
        // This is the only place that knows about Operation objects
        log.debug("Getting the other parameters for repopulation");
        Operation op = CreateAndLinkAdminAction.getOneOperation(getOpid());
        Issue issueObj = issueManager.getIssueObject(issue.getLong("id"));
        return op.getOtherParameters(issueObj, authcontext);
    }
    
    public Collection<Operation> getAllOperations() {
    	return CreateAndLinkAdminAction.getAllOperations();
    }

}
