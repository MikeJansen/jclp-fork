(function($) {

	$.fn.preCreateAndLink = function(issueId) {

		var preForm = this;
		var opSelect = $("#opSelect", this);
		var pidSelect = $("#pidSelect", this);
		var pidHidden = $("#pidHidden", this);
		var issueTypeSelect = $("#issueTypeSelect", this);
		var issueTypeHidden = $("#issueTypeHidden", this);
		var linkTypeSelect = $("#linkTypeSelect", this);
		var linkTypeHidden = $("#linkTypeHidden", this);

		// Function to find the next input element to focus on, starting from
		// the given input
		var findFocus = function(input) {
			var inputs = preForm.find("select, input");
			var index = inputs.index(input);
			for (var i = index + 1; i < inputs.length; i++) {
				var nextInput = $(inputs.get(i));
				if (!nextInput.is(":disabled")) {
					nextInput.focus();
					break;
				}
			}
		}

		// Function to update the options for the issue type select
		var updateValues = function(issueTypes) {
			issueTypeSelect.empty();
			$.each(issueTypes, function(n, issueType) {
				issueTypeSelect.append("<option value='" + issueType.id + "'>" + issueType.name + "</option>");
			});
		}

		// Function to retrieve the available issue types for a given
		// project from the REST api
		var getIssueTypes = function(projectId) {
			var issueTypes;
			$.ajax({
				type: "GET",
				url: contextPath + "/rest/createandlink/1.0/issuetypes",
				data: {
					pid: projectId
				},
				dataType: "json",
				async: false,
				success: function(data) {
					issueTypes = data;
				}
			});
			return issueTypes;
		}

		// Function to retrieve the issue types from the cache, or the
		// REST api, and update the issue type select list
		var updateIssueTypesList = function(projectId) {
			var issueTypes = pidSelect.data(projectId);
			// The cache may be invalid if new issue types were defined? Seems to be updated
			if (!issueTypes) {
				issueTypes = getIssueTypes(projectId);
				pidSelect.data(projectId, issueTypes);
			}
			updateValues(issueTypes);
		}
		
		// Displays the appropriate issue type based on the current operation
		var displayIssueType = function(op) {
			if (op.issuetype != "Choose") {
				issueTypeSelect.val(op.issuetype);
				issueTypeSelect.attr("disabled", true);
				issueTypeHidden.attr("disabled", false);
				issueTypeHidden.val(op.issuetype);
			} else {
				issueTypeSelect.attr("disabled", false);
				issueTypeHidden.attr("disabled", true);
			}
		}
		
		// Function to update form elements based on the currently
		// selected create and link operation
		var updateForm = function(op) {
			$("#otherParams").empty();
			$("#otherParams").html(op.otherParams);
			$("#formTitle").text(op.text);
			pidSelect.attr("disabled", false);
			pidHidden.attr("disabled", true);
			linkTypeSelect.attr("disabled", false);
			linkTypeHidden.attr("disabled", true);
			if (op.pid != "Choose") {
				pidSelect.val(op.pid);
				pidSelect.attr("disabled", true);
				pidHidden.attr("disabled", false);
				pidHidden.val(op.pid);
			}
			updateIssueTypesList(pidSelect.val());
			displayIssueType(op);
			if (op.linktype != "Choose") {
				var linkVal = op.linktype + "_" + op.linkdirection;
				linkTypeSelect.val(linkVal);
				linkTypeSelect.attr("disabled", true);
				linkTypeHidden.attr("disabled", false);
				linkTypeHidden.val(linkVal);
			}
		}

		// Retrieve the available create and link operations, initialize
		// the form and bind the change events
		$.ajax({
			type: "GET",
			url: contextPath + "/rest/createandlink/1.0/operations",
			data: {
				id: issueId
			},
			dataType: "json",
			success: function(data) {
				$.each(data, function(n, op) {
					if (n == 0) {
						updateForm(op);
					}
					opSelect.append("<option value='" + op.opid + "'>" + op.text + "</option>");
					// Cache each operation with the operation select element
					opSelect.data(op.opid, op);
				});
				opSelect.focus();
				// This change requires us to update the form
				opSelect.change(function() {
					var op = opSelect.data(opSelect.val());
					updateForm(op);
					findFocus($(this));
				});
				// This change requires us to update the issue types select
				pidSelect.change(function() {
					updateIssueTypesList(pidSelect.val());
					displayIssueType(opSelect.data(opSelect.val()));
					findFocus($(this));
				});
				issueTypeSelect.change(function() {
					findFocus($(this));
				});
				linkTypeSelect.change(function() {
					findFocus($(this));
				});
				// This form is left in a dirty state after initialization.  
				// Is there a cleaner approach to resetting this (no pun intended)? -JMP
				$.data(preForm[0], AJS.DIRTY_FORM_VALUE, preForm.find(":input[name!=atl_token]").serialize());
			}
		});

		return this;
	};

})(jQuery)