<%@ taglib prefix="ww" uri="webwork" %>
<%@ taglib prefix="aui" uri="webwork" %>
<%@ taglib prefix="page" uri="sitemesh-page" %>
<%@ page import="com.atlassian.jira.ComponentManager" %>
<%@ page import="com.atlassian.jira.web.action.util.FieldsResourceIncluder" %>
<html>
<%-- This file is a modified copy of createissue-details.jsp --%>
<head>
    <ww:if test="ableToCreateIssueInSelectedProject == 'true'"><meta content="genericaction" name="decorator" /></ww:if>
    <ww:else><meta content="error" name="decorator" /></ww:else>
    <title>Create and Link</title>
    <content tag="section">find_link</content>
    <%
        final FieldsResourceIncluder fieldResourceIncluder = ComponentManager.getComponentInstanceOfType(FieldsResourceIncluder.class);
        fieldResourceIncluder.includeFieldResourcesForCurrentUser();
    %>
</head>
<body class="type-a">
<ww:if test="ableToCreateIssueInSelectedProject == 'true'">
    <div class="content intform">
        <page:applyDecorator id="issue-create" name="auiform">
            <page:param name="action">CreateAndLink.jspa</page:param>
            <page:param name="submitButtonName">Create</page:param>
            <page:param name="submitButtonText"><ww:property value="submitButtonName" escape="false" /></page:param>
            <page:param name="cancelLinkURI"><ww:url value="'default.jsp'" atltoken="false"/></page:param>
            <page:param name="isMultipart">true</page:param>

            <aui:component template="formHeading.jsp" theme="'aui'">
                <aui:param name="'text'">Create and Link</aui:param>
            </aui:component>

            <aui:component template="formDescriptionBlock.jsp" theme="'aui'">
                <aui:param name="'text'">Create a new issue and add a <ww:property value="./linktypename"/> link to the parent issue <a href="<ww:property value="applicationProperties/string('jira.baseurl')"/>/browse/<ww:property value="/issue/string('key')"/>"><ww:property value="/issue/string('key')"/></a>&nbsp;[<ww:property value="/issue/string('summary')"/>]</aui:param>
            </aui:component>

            <page:applyDecorator name="auifieldgroup">
                <aui:component id="'project-name'" label="text('issue.field.project')" name="'project/string('name')'" template="formFieldValue.jsp" theme="'aui'" />
            </page:applyDecorator>

            <aui:component name="'pid'" template="hidden.jsp" theme="'aui'" />
            <aui:component name="'issuetype'" template="hidden.jsp" theme="'aui'" />
            <%-- Pass in various hidden variables related to Create and Link --%>
            <aui:component name="'linktype'" template="hidden.jsp" theme="'aui'" />
            <aui:component name="'linkdirection'" template="hidden.jsp" theme="'aui'" />
            <aui:component name="'id'" template="hidden.jsp" theme="'aui'" />
            <aui:component name="'arguments'" template="hidden.jsp" theme="'aui'" />

            <page:applyDecorator name="auifieldgroup">
                <aui:component id="'issue-type'" label="text('issue.field.issuetype')" name="'issueType'" template="formIssueType.jsp" theme="'aui'">
                    <aui:param name="'issueType'" value="/constantsManager/issueType(issuetype)" />
                </aui:component>
            </page:applyDecorator>

            <ww:component template="issuefields.jsp" name="'createissue'">
                <ww:param name="'displayParams'" value="/displayParams"/>
                <ww:param name="'issue'" value="/issueObject"/>
                <ww:param name="'tabs'" value="/fieldScreenRenderTabs"/>
                <ww:param name="'errortabs'" value="/tabsWithErrors"/>
                <ww:param name="'selectedtab'" value="/selectedTab"/>
                <ww:param name="'ignorefields'" value="/ignoreFieldIds"/>
                <ww:param name="'create'" value="'true'"/>
            </ww:component>

            <jsp:include page="/includes/panels/updateissue_comment.jsp" />

        </page:applyDecorator>
    </div>
</ww:if>
<ww:else>
    <page:applyDecorator name="auierrorpanel">
        <page:param name="title"><ww:text name="'panel.errors'"/></page:param>
		<%@ include file="/includes/createissue-notloggedin.jsp" %>
    </page:applyDecorator>
</ww:else>
</body>
</html>
